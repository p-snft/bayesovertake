#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <dai/evidence.h>
#include <dai/factorgraph.h>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

using namespace dai;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum vI {   follow = 0,
              overtake = 1,
                  noVI = -1 };

    enum vB { noFlash = 0,
                flash = 1,
                 noVB = -1 };

    enum vLA {     left = 0,
               streight = 1,
                  right = 2,
                  noVLA = -1 };

    enum vLO { accelerate = 0,
                   cruise = 1,
                    brake = 2,
                    noVLO = -1 };

private:
    Ui::MainWindow *ui;

    Evidence::Observation o;


    Var I; // intend overtaking
    Factor pI;

    Var B; // flash turn signal
    Factor pBI;

    Var LA; // lateral movement
    Factor pLAI;

    Var LO; // longitudinal movement
    Factor pLOI;

    FactorGraph network;

    Factor probabilities;

public slots:
    void setBFlash()   { setObserable(B,flash); }
    void setBNoFlash() { setObserable(B,noFlash); }
    void setBUnknown() { setObserable(B,noVB); }

    void setLALeft()     { setObserable(LA,left); }
    void setLAStreight() { setObserable(LA,streight); }
    void setLARight()    { setObserable(LA,right); }
    void setLAUnknown()  { setObserable(LA,noVLA); }


    void setLOAccelerate() { setObserable(LO,accelerate); }
    void setLOCruise()     { setObserable(LO,cruise); }
    void setLOBrake()      { setObserable(LO,brake); }
    void setLOUnknown()    { setObserable(LO,noVLO); }

private:
    void setObserable(Var obs, int state);

    void updateP();

    Real pIDerived;

    Real probability(Evidence::Observation of);

    Real probability(Evidence::Observation of,
                     Evidence::Observation when);
};

#endif // MAINWINDOW_HPP
