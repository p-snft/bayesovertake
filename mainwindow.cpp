#include "mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    I(0,2),
    pI( {{I}}, {{ 0.3, 0.7}} ),
    B(1,2),
    pBI( VarSet(B,I) ),
    LA(2,3),
    pLAI(( VarSet(LA,I) )),
    LO(3,3),
    pLOI(( VarSet(LO,I) ))
{
    ui->setupUi(this);
    connect(ui->B_flash,SIGNAL(clicked()),this,SLOT(setBFlash()));
    connect(ui->B_noFlash,SIGNAL(clicked()),this,SLOT(setBNoFlash()));
    connect(ui->B_unknown,SIGNAL(clicked()),this,SLOT(setBUnknown()));


    connect(ui->LA_left,SIGNAL(clicked()),this,SLOT(setLALeft()));
    connect(ui->LA_right,SIGNAL(clicked()),this,SLOT(setLARight()));
    connect(ui->LA_streight,SIGNAL(clicked()),this,SLOT(setLAStreight()));
    connect(ui->LA_unknown,SIGNAL(clicked()),this,SLOT(setLAUnknown()));


    connect(ui->LO_accelerate,SIGNAL(clicked()),this,SLOT(setLOAccelerate()));
    connect(ui->LO_brake,SIGNAL(clicked()),this,SLOT(setLOBrake()));
    connect(ui->LO_cruise,SIGNAL(clicked()),this,SLOT(setLOCruise()));
    connect(ui->LO_unknown,SIGNAL(clicked()),this,SLOT(setLOUnknown()));


    pBI.set(I.states()* noFlash+follow  ,0.9);
    pBI.set(I.states()* noFlash+overtake,0.2);
    pBI.set(I.states()*   flash+follow  ,0.1);
    pBI.set(I.states()*   flash+overtake,0.8);

    pLAI.set(I.states()*     left+follow  ,0.1);
    pLAI.set(I.states()*     left+overtake,0.5);
    pLAI.set(I.states()* streight+follow  ,0.8);
    pLAI.set(I.states()* streight+overtake,0.2);
    pLAI.set(I.states()*    right+follow  ,0.1);
    pLAI.set(I.states()*    right+overtake,0.3);

    pLOI.set(I.states()* accelerate+follow  ,0.1);
    pLOI.set(I.states()* accelerate+overtake,0.7);
    pLOI.set(I.states()*     cruise+follow  ,0.2);
    pLOI.set(I.states()*     cruise+overtake,0.2);
    pLOI.set(I.states()*      brake+follow  ,0.7);
    pLOI.set(I.states()*      brake+overtake,0.1);

    network = FactorGraph(std::vector<Factor>({{pI, pBI, pLAI, pLOI}}));

    for( size_t i = 0; i < network.nrFactors(); i++ ) {
        probabilities *= network.factor( i );
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setObserable(Var obs, int state) {
    if (state < 0) {
        o.erase(obs);
    } else {
        o[obs] = state;
    }
    updateP();
    QString p;
    p.setNum(pIDerived);
    ui->probability->setText(p);
}

void MainWindow::updateP()
{
    o.erase(I);
    Evidence::Observation of;
    of[ I] = overtake;
    pIDerived = probability(of,o);
}

Real MainWindow::probability(Evidence::Observation of)
{
    VarSet vars = VarSet();

    size_t index = 0;
    size_t states = 1;
    for (auto o : of) {
        vars = vars | o.first;
        index = states*o.second+index;
        states *= o.first.states();
    }

    return probabilities.marginal(vars)[index];
}

Real MainWindow::probability(Evidence::Observation of,
                             Evidence::Observation when)
{
    for (auto c : when) {
        auto it = of.find(c.first);
        if (it != of.end()) {
            if (it->second != c.second) {
                return 0;
            }
        } else {
            of[c.first] = c.second;
        }
    }
    Real nom = probability(of);
    Real denom = probability(when);

    return nom/denom;
}
